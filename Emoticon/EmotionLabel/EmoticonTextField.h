//
//  EmoticonTextField.h
//  Emoticon
//
//  Created by Banu Desi Antoro on 6/17/14.
//  Copyright (c) 2014 Sebangsa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmoticonTextField : UITextField

@property (nonatomic, strong) NSArray *matchArray;

@end
