//
//  main.m
//  Emoticon
//
//  Created by Banu Desi Antoro on 6/17/14.
//  Copyright (c) 2014 Sebangsa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
