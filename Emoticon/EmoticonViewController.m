//
//  EmoticonViewController.m
//  Emoticon
//
//  Created by Banu Desi Antoro on 6/17/14.
//  Copyright (c) 2014 Sebangsa. All rights reserved.
//

#import "EmoticonViewController.h"
#import "EmotionLabel.h"
#import "EmoticonTextField.h"

@interface EmoticonViewController () {
    EmotionLabel *emotionLabel;
    EmoticonTextField *emoticonTextField;
}
@end

@implementation EmoticonViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    UILabel *textLabel = [[UILabel alloc] init];
//    CGRect textRect = textLabel.frame;
//    textRect.origin.x = self.view.frame.size.width/2;
//    textRect.origin.y = self.view.frame.size.height/2;
//    textRect.size.width = 40;
//    textRect.size.height = 20;
//    textLabel.frame = textRect;
//    [textLabel setText:@"hai"];
//    
//    [self.view addSubview:textLabel];
    
    emotionLabel = [[EmotionLabel alloc] init];
    [emotionLabel setFont:[UIFont systemFontOfSize:20.f]];
    [emotionLabel setTextColor:[UIColor darkGrayColor]];
    [emotionLabel setShadowColor:[UIColor lightGrayColor]];
    [emotionLabel setShadowOffset:CGSizeMake(0, 1)];
    NSArray *arr = [[NSArray alloc] initWithContentsOfFile:
                    [[NSBundle mainBundle] pathForResource:@"MyEmoji"
                                                    ofType:@"plist"]];
    [emotionLabel setMatchArray:arr];
    [emotionLabel setFrame:CGRectMake(10, 10, 300, 500)];
    [emotionLabel setLineBreakMode:NSLineBreakByCharWrapping];
    [emotionLabel setNumberOfLines:0];
    [self.view addSubview:emotionLabel];
    
    [emotionLabel setText:@"[a] :a:"];
    
    emoticonTextField = [[EmoticonTextField alloc] init];
    [emoticonTextField setFont:[UIFont systemFontOfSize:20.f]];
    [emoticonTextField setTextColor:[UIColor darkGrayColor]];
    
    [emoticonTextField setMatchArray:arr];
    [emoticonTextField setFrame:CGRectMake(10, 20, 300, 500)];
    [emoticonTextField setBackgroundColor:[UIColor grayColor]];

    [self.view addSubview:emoticonTextField];
    
    [emoticonTextField setText:@"[a] :a:"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
