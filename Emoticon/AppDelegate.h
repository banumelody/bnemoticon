//
//  AppDelegate.h
//  Emoticon
//
//  Created by Banu Desi Antoro on 6/17/14.
//  Copyright (c) 2014 Sebangsa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class EmoticonViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    UIWindow *_window;
    EmoticonViewController *_emoticonController;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) EmoticonViewController *emoticonController;

@end

